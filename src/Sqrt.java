import java.util.Scanner;
public class Sqrt {
    static int x;
    static boolean a = true;

    static int input() {
        while(a == true){
            Scanner kb = new Scanner(System.in);
            x = kb.nextInt();
            if(x<0){
                System.out.println("Try Again!");
            }else{
                a = false;
            }
        }
        return x;
    }

    static int mySqrt(int x){
        if (x == 0){
            return 0;
        }
        int left = 1, right = x;
        while(left < right){
            int mid = left + (right-left)/2;
            if(mid == x/mid){
                return mid;
            }else if (mid < x / mid){
                left = mid +1;
            }else{
                right = mid -1;
            }
        }
        return right-1;
    }

    public static void main(String[] args) {
        input();
        int result = mySqrt(x);
        System.out.println(result);
    }
}
